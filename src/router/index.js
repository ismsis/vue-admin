import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    redirect: '/Home/users'
  },
  {
    path: '/Home',
    name: 'Home',
    component: Home,
    redirect: '/Home/users',
    children: [{
        path: 'users',
        name: 'users',
        component: () => import('../views/users/users.vue')
      },
      {
        path: 'roles',
        name: 'roles',
        component: () => import('../views/rights/roles.vue')
      },
      {
        path: 'rights',
        name: 'rights',
        component: () => import('../views/rights/rights.vue')
      },
      {
        path: 'goods',
        name: 'goods',
        component: () => import('../views/goods/goods.vue')
      },
      {
        path: 'addgoods',
        name: 'addgoods',
        component: () => import('../views/goods/addgoods.vue')
      },
      {
        path: 'params',
        name: 'params',
        component: () => import('../views/goods/params.vue')
      },
      {
        path: 'categories',
        name: 'categories',
        component: () => import('../views/goods/categories.vue')
      },
      {
        path: 'orders',
        name: 'orders',
        component: () => import('../views/orders/orders.vue')
      },
      {
        path: 'reports',
        name: 'reports',
        component: () => import('../views/reports/reports.vue')
      },
    ]
  },
  {
    path: '/Login',
    name: 'Login',
    meta: {
      notAuthorization: true
    },
    component: () => import('../views/Login.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})



//解决路由重复点击报错问题
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  if (typeof(location) == "string") {
    var Separator = "&";
    if (location.indexOf('?') == -1) {
      Separator = '?';
    }
    location = location + Separator + "random=" + Math.random();
  }
  return routerPush.call(this, location).catch(error => error)
}

router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && !store.state.token) next({
    name: 'Login',
    query: {
      redirect: to.name
    }
  })
  else next()
})


export default router