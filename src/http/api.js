import http from './index'
import {
  Message
} from 'element-ui'

//登录
export const login = async (obj) => {
  // 调用index.js中方法
  const res = await http('login', 'post', {}, obj);
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res
}

//获取权限菜单表
export const getMenus = async (route) => {
  // 调用index.js中方法
  const res = await http('menus', 'GET', {}, {}, route)
  return res;
}
// ---------------------------------------------------------------------------------
// 用户管理
//获取用户列表
export const Users = async (obj) => {
  // 调用index.js中方法
  const res = await http('users', 'GET', obj, {}, )
  return res;
}
// 添加用户
export const AddUser = async (obj) => {
  // 调用index.js中方法
  const res = await http('users', 'POST', {}, obj, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 修改用户状态
export const UserState = async (uid, state) => {
  // 调用index.js中方法
  const res = await http('users/' + uid + '/state/' + state, 'put', {}, {}, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 删除单个用户
export const Deleteuser = async (id) => {
  // 调用index.js中方法
  const res = await http('users/' + id, 'delete', {}, {}, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 获取所有角色
export const role = async () => {
  // 调用index.js中方法
  const res = await http('roles', 'get', {}, {}, )
  return res;
}
//根据 ID 查询用户信息
export const UserMsg0 = async (id) => {
  // 调用index.js中方法
  const res = await http('users/' + id, 'get', {}, {}, )
  return res;
}
//根据 ID 查询用户信息
export const UserMsg = async (id) => {
  // 调用index.js中方法
  const res = await http('roles/' + id, 'get', {}, {}, )
  return res;
}
//分配用户角色
export const userrole = async (id, obj) => {
  // 调用index.js中方法
  const res = await http('users/' + id + '/role', 'put', {}, obj, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 编辑用户提交
export const UserSubmit = async (id, obj) => {
  // 调用index.js中方法
  const res = await http('users/' + id, 'put', {}, obj, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// ---------------------------------------------------------------------------
// 角色管理
//获取角色列表
export const Roles = async () => {
  // 调用index.js中方法
  const res = await http('roles', 'get', {}, {}, )
  return res;
}
//添加角色
export const addRoleApi = async (obj) => {
  console.log(obj)
  // 调用index.js中方法
  const res = await http('roles', 'post', {}, obj, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 编辑提交角色
export const editorRoleApi = async (id, obj) => {
  // 调用index.js中方法
  const res = await http('roles/' + id, 'put', {}, obj, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 删除角色
export const deleteRoleApi = async (id) => {
  // 调用index.js中方法
  const res = await http('roles/' + id, 'delete', {}, {}, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 删除角色指定权限
export const removeRoleApi = async (rid, rrid) => {
  console.log(rid, rrid)
  // 调用index.js中方法
  const res = await http('roles/' + rid + '/rights/' + rrid, 'delete', {}, {}, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 获取所有角色权限树状
export const getRoleApi = async () => {
  // 调用index.js中方法
  const res = await http('rights/tree', 'get', {}, {}, )
  // 弹窗提示
  // Message({
  //   type: 'success',
  //   message: res.meta.msg
  // })
  return res;
}
// 角色授权
export const authRoleApi = async (rid, obj) => {
  // 调用index.js中方法
  const res = await http('roles/' + rid + '/rights', 'post', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 获取所有角色权限列表
export const getlistRoleApi = async () => {
  // 调用index.js中方法
  const res = await http('rights/list', 'get', {}, {}, )
  // 弹窗提示
  // Message({
  //   type: 'success',
  //   message: res.meta.msg
  // })
  return res;
}
// -----------------------------------------------------------------------------
// *商品管理
// 商品列表数据
export const goodslist = async (obj) => {
  // 调用index.js中方法
  const res = await http('goods', 'GET', obj, {}, )
  return res;
}
// 删除商品
export const delGoodsApi = async (id) => {
  // 调用index.js中方法
  const res = await http('goods/' + id, 'delete', {}, {}, )
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 根据 ID 查询商品
export const queryGoodsApi = async (id) => {
  // 调用index.js中方法
  const res = await http('goods/' + id, 'get', {}, {}, )
  return res;
}
// 编辑提交商品
export const submitGoodsApi = async (id, obj) => {
  // 调用index.js中方法
  console.log(id, obj);
  const res = await http('goods/' + id, 'put', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 商品数据列表
export const categoriesApi = async (obj) => {
  // 调用index.js中方法
  const res = await http('categories/', 'get', obj, {})
  return res;
}
// 获取参数列表
export const categoriesListApi = async (id, obj) => {
  // 调用index.js中方法
  const res = await http('categories/' + id + '/attributes', 'get', obj, {})
  return res;
}
// 添加商品
export const addGoodsAddApi = async (obj) => {
  console.log(obj)
  // 调用index.js中方法
  const res = await http('goods', 'post', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 删除分类
export const delCategoriesApi = async (id, obj) => {
  // 调用index.js中方法
  const res = await http('categories/' + id, 'delete', {}, {})
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 添加分类
export const classGoodsApi = async (obj) => {
  console.log(obj);
  // 调用index.js中方法
  const res = await http('categories', 'post', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 编辑提交分类
export const suGoodsApi = async (id, obj) => {
  // 调用index.js中方法
  const res = await http('categories/' + id, 'put', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 删除分类
export const delfenleiApi = async (id, attrid) => {
  // 调用index.js中方法
  const res = await http('categories/' + id + '/attributes/' + attrid, 'delete', {}, {})
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 添加动态静态参数
export const shezhiApi = async (id, obj) => {
  // 调用index.js中方法
  const res = await http('categories/' + id + '/attributes', 'post', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// 编辑提交参数
export const compileApi = async (id, attrid, obj) => {
  // 调用index.js中方法
  const res = await http('categories/' + id + '/attributes/' + attrid, 'put', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// -------------------------------------------------------
// 获取订单列表数据
export const getOrdersApi = async (obj) => {
  // 调用index.js中方法
  const res = await http('orders', 'get', obj, {})
  return res;
}
// 查看订单详情
export const lookOrdersApi = async (id) => {
  // 调用index.js中方法
  const res = await http('orders/' + id, 'get', {}, {})
  return res;
}
// 修改订单
export const changOrdersApi = async (id, obj) => {
  console.log(obj)
  // 调用index.js中方法
  const res = await http('orders/' + id, 'put', {}, obj)
  // 弹窗提示
  Message({
    type: 'success',
    message: res.meta.msg
  })
  return res;
}
// -------------------------------------------------------
// 获取折线图数据
export const reportsApi = async (id) => {
  // 调用index.js中方法
  const res = await http('reports/type/1', 'get', {}, {})
  return res;
}