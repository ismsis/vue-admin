import axios from 'axios'
import {
  Message
} from 'element-ui'
import store from '@/store'
import router from '@/router'
//开发环境
const baseURL = 'http://localhost:8889/api/private/v1/'
//测试环境
// const baseURL = 'http://localhost:8889/api/private/v1/'
//生产环境
// const baseURL = 'http://localhost:8889/api/private/v1/'





// 添加请求拦截器
axios.interceptors.request.use(function(config) {

  // 在发送请求之前做些什么
  // 判断vuex中state中的token
  if (store.state.token) {
    // 添加到响应头中
    config.headers.Authorization = store.state.token
  } else {
    if (!router.history.current.meta.notAuthorization) {
      router.push({
        name: 'Login'
      })
    }
  }
  return config;
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function(res) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  // 判断状态码
  if (res.data.meta.status === 400) {
    router.push({
      name: 'Login',
      query: {
        redirect: router.history.current.name
      }
    })
    // 调用vuex中的方法修改用户数据为空
    store.commit('USERMSG', {
      username: '',
      token: ''
    })
  }
  return res;
}, function(error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error);
});


function http(url, method = 'GET', params = {}, data = {}) {
  return axios({
    url: baseURL + url,
    data,
    method,
    params
  }).then(res => {
    //   判断状态码
    if (res.status >= 200 && res.status < 300 || res.status == 304) {
      if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
        // Message.success(res.data.meta.msg)
        return res.data
      } else {
   
        // console.log(res.data.meta.msg);
        // if (res.data.meta.msg == '无效token') {
          Message.error(res.data.meta.msg)
        // }

      }
    } else {}
  }).catch(err => {
    console.log(err)
  })
}

export default http;
//但组件引入
// export {http};