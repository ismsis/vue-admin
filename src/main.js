import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//引入安装的清除默认样式
import 'reset-css';
//引入element
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
//引入http
import http from '@/http'
Vue.prototype.$http = http

// 引入echarts折线数据图
import * as echarts from 'echarts';
Vue.prototype.$echarts = echarts


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')